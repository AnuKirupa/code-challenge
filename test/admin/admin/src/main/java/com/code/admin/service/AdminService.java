package com.code.admin.service;

import java.util.List;

import com.code.admin.model.Stock;

public interface AdminService {
	
	List<Stock> viewStockDetails();
	String updateMarketStatus();
	String executeOrder(int stockId,int qty, int price);
	String updateMarketStatusByStockId(int stockId, String status);
}
