package com.code.customer.serviceimpl;

import java.util.ArrayList;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.code.customer.model.CustomerOrder;
import com.code.customer.model.CustomerOrderResponse;
import com.code.customer.model.OrderDto;
import com.code.customer.model.Stock;
import com.code.customer.repository.CustomerOrderRepository;
import com.code.customer.repository.CustomerRepository;
import com.code.customer.repository.StockRepository;
import com.code.customer.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerRepository cusRepo;
	@Autowired
	CustomerOrderRepository cusOrderRepo;
	@Autowired
	StockRepository stockRepo;

	@Override
	public String placeOrder(OrderDto reqOrderDto) {
		String status = "";
		Stock stock = stockRepo.findByStockId(reqOrderDto.getStockId());
		if (stock.getMarketStatus().equalsIgnoreCase("open")) {
			CustomerOrder cusOrder = new CustomerOrder();
			BeanUtils.copyProperties(reqOrderDto, cusOrder);
			cusOrderRepo.save(cusOrder);
			status = "Order Placed Successfully";
		} else
			status = "Market is Closed. You Can't Place Order";

		return status;
	}

	@Override
	public List<CustomerOrder> viewAllOrderStatus() {
		List<CustomerOrder> orderList = new ArrayList<CustomerOrder>();
		orderList = cusOrderRepo.findAll();
		return orderList;
	}
	@Override
	public List<CustomerOrderResponse> getAllOrdersByStockId(@RequestParam int stockId)
	{
		List<CustomerOrderResponse> ordersList=new ArrayList<>();
		List<CustomerOrder> ordersLst=new ArrayList<>();
		ordersLst=cusOrderRepo.getAllOrdersByStockId(stockId);
		for(CustomerOrder order : ordersLst) {
		CustomerOrderResponse order1=new CustomerOrderResponse();
		BeanUtils.copyProperties(order, order1);
		ordersList.add(order1);
		}
		return ordersList;
	}
	@Override
	public void UpdateCustomerOrder(@RequestBody CustomerOrderResponse customerOrder) {
		CustomerOrder custOrder=new CustomerOrder();
		BeanUtils.copyProperties(customerOrder, custOrder);
		cusOrderRepo.save(custOrder);
	}

	/*
	 * @Override public List<Stock> viewStockDetails() { List<Stock> inventoryList =
	 * new ArrayList<>(); inventoryList = stockRepo.findAll(); return inventoryList;
	 * }
	 */

	/*
	 * @Override public String updateMarketStatus() { List<Stock> stockLst =
	 * stockRepo.findAll(); for (Stock stock : stockLst) { if (stock.getQuantity()
	 * == 0) { stock.setMarketStatus("close"); stockRepo.save(stock); } else {
	 * stock.setMarketStatus("open"); stockRepo.save(stock); } }
	 * 
	 * return "success"; }
	 */
	
	/*
	 * @Override public String updateMarketStatusByStockId(int stockId, String
	 * status) { Stock stock=stockRepo.findByStockId(stockId);
	 * stock.setMarketStatus(status); stockRepo.save(stock); return
	 * "Market Status Updated Successfully"; }
	 */

	/*
	 * @Override public String executeOrder(int stockId, int qty, int price) {
	 * List<CustomerOrder> orderList = new ArrayList<CustomerOrder>();
	 * List<CustomerOrder> placedOrderList = new ArrayList<CustomerOrder>(); float
	 * totalOrderQty = 0; orderList = cusOrderRepo.getAllOrdersByStockId(stockId);
	 * for (CustomerOrder order : orderList) { if
	 * (order.getOrderType().equalsIgnoreCase("Limit") && order.getPrice() >= price
	 * || order.getOrderType().equalsIgnoreCase("Market")) { totalOrderQty =
	 * totalOrderQty + order.getQuantity(); placedOrderList.add(order);
	 * 
	 * } else { order.setStatus("Rejected"); cusOrderRepo.save(order);
	 * 
	 * } } for (CustomerOrder porder : placedOrderList) { float
	 * sum=(qty/totalOrderQty); float orderPer = (sum) * 100; int placeQty = (int)
	 * (porder.getQuantity() * (orderPer / 100)); porder.setStatus("accepted");
	 * porder.setAcceptedQty(placeQty); cusOrderRepo.save(porder); } return
	 * "success";
	 * 
	 * }
	 */
}
